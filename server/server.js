const express = require('express');
const path = require('path');
const socketio = require('socket.io');
const http = require('http');
const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3001;
const app = express();
let server = http.createServer(app);
app.use(express.static(publicPath));
// IO = this is the comunication with the backend
module.exports.io = socketio(server);
require('./sockets/socket')
server.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Server on port ${ port }`);

});