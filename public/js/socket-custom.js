const socket = io();
socket.on('connect', () => {
    console.log('Connect to socket')
})
socket.on('disconnect', () => {
    console.log("Faild connection with server")
})
socket.emit('sendMessage', {
    user: 'Rony',
    message: 'HELLO WORLD'
}, response => {
    console.log(response)
})
socket.on('sendMessage', message => {
    console.log(message)
})